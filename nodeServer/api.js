var express = require('express'),
    router = express.Router(),
    db = require('./db.js'),
    login = require('./login.js'),
    TYPES = require('tedious').TYPES;


// --------------------------------------------------------
// KORISNICI
// --------------------------------------------------------
// select
router.get('/Korisnik', function (req, res) {
    var conn = db.createConnection();
    var request = db.createRequest("Sigurnost.spApplicationUser_Select", conn);

    request.addParameter('KorisnikPkUsera', TYPES.Int, req.query.KorisnikPkUsera);
    request.addParameter('PkUsera', TYPES.Int, req.query.PkUsera);
    db.execStoredProc(request, conn, res, '[]');
});

// insert
router.post('/Korisnik', function (req, res) {
    var conn = db.createConnection();
    var request = db.createRequest("Sigurnost.spApplicationUser_Insert", conn);

    request.addParameter('ImeUsera', TYPES.NVarChar, req.body.user.name);
    request.addParameter('PrezimeUsera', TYPES.NVarChar, req.body.user.surname);
    request.addParameter('AktivanDaNe', TYPES.Bit, req.body.user.active);
    request.addParameter('LoginName', TYPES.NVarChar, req.body.user.username);
    request.addParameter('LoginPassword', TYPES.NVarChar, req.body.user.password.length > 0 ? login.encrypt(req.body.user.password): null);
    request.addParameter('ObaveznaIzmjenaLozinkeDaNe', TYPES.Bit, req.body.user.passwordChangeNeeded);
    request.addParameter('Email', TYPES.NVarChar, req.body.user.email);
    request.addParameter('BusinessPartnerName', TYPES.NVarChar, req.body.user.businessPartnerName);
    request.addParameter('BusinessPartnerId', TYPES.NVarChar, req.body.user.businessPartnerId);
    request.addParameter('PkUsera', TYPES.Int, req.body.insertPk);
    request.addOutputParameter('KorisnikPkUsera', TYPES.Int);

    db.execStoredProc(request, conn, res, '{}');
});

// update
router.put('/Korisnik', function (req, res) {
    var conn = db.createConnection();
    var request = db.createRequest("Sigurnost.spApplicationUser_Update", conn);

    request.addParameter('KorisnikPkUsera', TYPES.Int, req.body.user.userId);
    request.addParameter('ImeUsera', TYPES.NVarChar, req.body.user.name);
    request.addParameter('PrezimeUsera', TYPES.NVarChar, req.body.user.surname);
    request.addParameter('AktivanDaNe', TYPES.Bit, req.body.user.active);
    request.addParameter('LoginName', TYPES.NVarChar, req.body.user.username);
    request.addParameter('ObaveznaIzmjenaLozinkeDaNe', TYPES.Bit, req.body.user.passwordChangeNeeded);
    request.addParameter('Email', TYPES.NVarChar, req.body.user.email);
    request.addParameter('BusinessPartnerName', TYPES.Int, req.body.user.businessPartnerName);
    request.addParameter('BusinessPartnerId', TYPES.NVarChar, req.body.user.businessPartnerId);
    request.addParameter('PkUsera', TYPES.Int,req.body.changePk);
    request.addParameter('RowVersion', TYPES.NVarChar, req.body.user.rowVersion);

    db.execStoredProc(request, conn, res, '{}');
});

// delete
router.delete('/Korisnik', function (req, res) {
    var conn = db.createConnection();
    var request = db.createRequest("Sigurnost.spApplicationUser_Delete", conn);

    request.addParameter('KorisnikPkUsera', TYPES.Int, req.body.user.userId);
    request.addParameter('PkUsera', TYPES.Int, req.body.deletePk);

    db.execStoredProc(request, conn, res, '{}');
});

router.put('/KorisnikIzmjenaObaveznaLozinkeSVI', function (req, res) {
    var conn = db.createConnection();
    var request = db.createRequest("Sigurnost.spApplicationUserObaveznaIzmjenaLozinkeSVI_Update", conn);
    request.addParameter('PkUsera', TYPES.Int, req.body.PkUsera);

    db.execStoredProc(request, conn, res, '{}');
});

router.put('/KorisnikIzmjenaObaveznaLozinke', function (req, res) {
    var conn = db.createConnection();
    var request = db.createRequest("Sigurnost.spApplicationUserObaveznaIzmjenaLozinke_Update", conn);
    request.addParameter('KorisnikPkUsera', TYPES.Int, req.body.KorisnikPkUsera);
    request.addParameter('ObaveznaIzmjenaLozinkeDaNe', TYPES.Bit, req.body.ObaveznaIzmjenaLozinkeDaNe);
    request.addParameter('RowVersion', TYPES.NVarChar, req.body.RowVersion);
    request.addParameter('PkUsera', TYPES.Int, req.body.PkUsera);

    db.execStoredProc(request, conn, res, '{}');
});

router.put('/KorisnikAktivan', function (req, res) {
    var conn = db.createConnection();
    var request = db.createRequest("Sigurnost.spApplicationUserAktivan_Update", conn);
    request.addParameter('KorisnikPkUsera', TYPES.Int, req.body.KorisnikPkUsera);
    request.addParameter('AktivanDaNe', TYPES.Bit, req.body.AktivanDaNe);
    request.addParameter('RowVersion', TYPES.NVarChar, req.body.RowVersion);
    request.addParameter('PkUsera', TYPES.Int, req.body.PkUsera);

    db.execStoredProc(request, conn, res, '{}');
});

//Izmjena lozinke 
router.put('/IzmjenaLozinke', function (req, res) {
    var conn = db.createConnection();
    var request = db.createRequest("Sigurnost.spApplicationUser_ChangePassword", conn);

    request.addParameter('KorisnikPkUsera', TYPES.Int, req.body.user.userId);
    request.addParameter('StaraLozinka', TYPES.NVarChar, req.body.oldPassword);
    request.addParameter('NovaLozinka', TYPES.NVarChar, login.encrypt(req.body.user.password));

    db.execStoredProc(request, conn, res, '{}');
});

//Izmjena lozinke od strane admina
router.put('/IzmjenaLozinkeAdmin', function (req, res) {
    var conn = db.createConnection();
    var request = db.createRequest("Sigurnost.spApplicationUser_AdminChangePassword", conn);

    request.addParameter('KorisnikPkUsera', TYPES.Int, req.body.KorisnikPkUsera);
    request.addParameter('NovaLozinka', TYPES.NVarChar, login.encrypt(req.body.NovaLozinka));
    request.addParameter('PkUsera', TYPES.Int, req.body.PkUsera);
    request.addParameter('RowVersion', TYPES.NVarChar, req.body.RowVersion);

    db.execStoredProc(request, conn, res, '{}');
});

// pripadajuce grupe
router.get('/KorisnikPripadajuceGrupe', function (req, res) {
    var conn = db.createConnection();
    var request = db.createRequest("Sigurnost.spApplicationUserPripadajuceGrupe_Select", conn);

    request.addParameter('KorisnikPkUsera', TYPES.Int, req.query.KorisnikPkUsera);
    request.addParameter('PkUsera', TYPES.Int, req.query.PkUsera);
    db.execStoredProc(request, conn, res, '[]');
});

// raspolozive grupe
router.get('/KorisnikRaspoloziveGrupe', function (req, res) {
    var conn = db.createConnection();
    var request = db.createRequest("Sigurnost.spApplicationUserRaspoloziveGrupe_Select", conn);

    request.addParameter('KorisnikPkUsera', TYPES.Int, req.query.KorisnikPkUsera);
    request.addParameter('PkUsera', TYPES.Int, req.query.PkUsera);
    db.execStoredProc(request, conn, res, '[]');
});

router.post('/KorisnikDodajUGrupu', function (req, res) {
    var conn = db.createConnection();
    var request = db.createRequest("Sigurnost.spApplicationUserApplicationUserGroup_Insert", conn);

    request.addParameter('KorisnikPkUsera', TYPES.Int, req.body.KorisnikPkUsera);
    request.addParameter('PkApplicationUserGroup', TYPES.Int, req.body.groupPk);
    request.addParameter('PkUsera', TYPES.Int, req.body.PkUsera);
    request.addOutputParameter('PkApplicationUserApplicationUserGroup', TYPES.Int);

    db.execStoredProc(request, conn, res, '{}');
});

router.delete('/KorisnikIzbrisiIzGrupe', function (req, res) {
    var conn = db.createConnection();
    var request = db.createRequest("Sigurnost.spApplicationUserApplicationUserGroup_Delete", conn);

    request.addParameter('PkApplicationUserApplicationUserGroup', TYPES.Int, req.body.PkApplicationUserApplicationUserGroup);
    request.addParameter('PkUsera', TYPES.Int, req.body.PkUsera);

    db.execStoredProc(request, conn, res, '{}');
});

router.get('/AdminDaNe', function (req, res) {
    var conn = db.createConnection();
    var request = db.createRequest("Sigurnost.spAdminDaNe", conn);

    request.addParameter('PkUsera', TYPES.Int, req.query.PkUsera);
    request.addOutputParameter('AdminDaNE', TYPES.Bit);
    db.execStoredProc(request, conn, res, '[]');
});

// dohvat svih komponenti na koje korisnik ima pravo
router.get('/korisnikKomponente', function (req, res) {
    var conn = db.createConnection();
    var request = db.createRequest("Sigurnost.spApplicationUserDozvoljeneKomponente", conn);

    request.addParameter('PkUsera', TYPES.Int, req.query.PkUsera);
    db.execStoredProc(request, conn, res, '[]');
});

// --------------------------------------------------------
// GRUPE KORISNIKA
// --------------------------------------------------------
// select
router.get('/GrupeKorisnika', function (req, res) {
    var conn = db.createConnection();
    var request = db.createRequest("Sigurnost.spApplicationUserGroup_Select", conn);

    request.addParameter('PkApplicationUserGroup', TYPES.Int, req.query.PkApplicationUserGroup);
    request.addParameter('PkUsera', TYPES.Int, req.query.PkUsera);
    db.execStoredProc(request, conn, res, '[]');
});

// insert
router.post('/GrupeKorisnika', function (req, res) {
    var conn = db.createConnection();
    var request = db.createRequest("Sigurnost.spApplicationUserGroup_Insert", conn);

    request.addParameter('IDApplicationUserGroup', TYPES.NVarChar, req.body.groupId);
    request.addParameter('NazivApplicationUserGroup', TYPES.NVarChar, req.body.groupName);
    request.addParameter('AktivnaGrupaDaNe', TYPES.Bit, req.body.active);
    request.addParameter('GrupaVidljivaKorisnicima', TYPES.Bit, req.body.visible);
    request.addParameter('SuperUserDaNe', TYPES.Bit, req.body.SuperUserDaNe);
    request.addParameter('PkUsera', TYPES.Int, 3);
    request.addOutputParameter('PkApplicationUserGroup', TYPES.Int);

    db.execStoredProc(request, conn, res, '{}');
});

// update
router.put('/GrupeKorisnika', function (req, res) {
    var conn = db.createConnection();
    var request = db.createRequest("Sigurnost.spApplicationUserGroup_Update", conn);

    request.addParameter('PkApplicationUserGroup', TYPES.Int, req.body.groupPk);
    request.addParameter('IDApplicationUserGroup', TYPES.NVarChar, req.body.groupId);
    request.addParameter('NazivApplicationUserGroup', TYPES.NVarChar, req.body.groupName);
    request.addParameter('AktivnaGrupaDaNe', TYPES.Bit, req.body.active);
    request.addParameter('GrupaVidljivaKorisnicima', TYPES.Bit, req.body.visible);
    request.addParameter('SuperUserDaNe', TYPES.Bit, req.body.SuperUserDaNe);
    request.addParameter('PkUsera', TYPES.Int, req.body.PkUsera);
    request.addParameter('RowVersion', TYPES.NVarChar, req.body.rowVersion);
    db.execStoredProc(request, conn, res, '{}');
});

// delete
router.delete('/GrupeKorisnika', function (req, res) {
    var conn = db.createConnection();
    var request = db.createRequest("Sigurnost.spApplicationUserGroup_Delete", conn);

    request.addParameter('PkApplicationUserGroup', TYPES.Int, req.body.groupPk);
    request.addParameter('PkUsera', TYPES.Int, req.body.groupPk);

    db.execStoredProc(request, conn, res, '{}');
});

router.put('/GrupaKorisnikaAktivna', function (req, res) {
    var conn = db.createConnection();
    var request = db.createRequest("Sigurnost.spApplicationUserGroupAktivna_Update", conn);
    request.addParameter('PkApplicationUserGroup', TYPES.Int, req.body.PkApplicationUserGroup);
    request.addParameter('AktivnaGrupaDaNe', TYPES.Bit, req.body.AktivnaGrupaDaNe);
    request.addParameter('RowVersion', TYPES.NVarChar, req.body.RowVersion);
    request.addParameter('PkUsera', TYPES.Int, req.body.PkUsera);

    db.execStoredProc(request, conn, res, '{}');
});

// pripadajuci korisnici
router.get('/GrupeKorisnikaPripadajuciKorisnici', function (req, res) {
    var conn = db.createConnection();
    var request = db.createRequest("Sigurnost.spApplicationUserGroupPripadajuciKorisnici_Select", conn);

    request.addParameter('PkApplicationUserGroup', TYPES.Int, req.query.PkApplicationUserGroup);
    request.addParameter('PkUsera', TYPES.Int, req.query.PkUsera);
    db.execStoredProc(request, conn, res, '[]');
});

// raspolozivi korisnici
router.get('/GrupeKorisnikaRaspoloziviKorisnici', function (req, res) {
    var conn = db.createConnection();
    var request = db.createRequest("Sigurnost.spApplicationUserGroupRaspoloziviKorisnici_Select", conn);

    request.addParameter('PkApplicationUserGroup', TYPES.Int, req.query.PkApplicationUserGroup);
    request.addParameter('PkUsera', TYPES.Int, req.query.PkUsera);
    db.execStoredProc(request, conn, res, '[]');
});

// pripadajuce role
router.get('/GrupeKorisnikaPripadajuceRole', function (req, res) {
    var conn = db.createConnection();
    var request = db.createRequest("Sigurnost.spApplicationUserGroupPripadajuceRole_Select", conn);

    request.addParameter('PkApplicationUserGroup', TYPES.Int, req.query.PkApplicationUserGroup);
    request.addParameter('PkUsera', TYPES.Int, req.query.PkUsera);
    db.execStoredProc(request, conn, res, '[]');
});

// raspolozive role
router.get('/GrupeKorisnikaRaspoloziveRole', function (req, res) {
    var conn = db.createConnection();
    var request = db.createRequest("Sigurnost.spApplicationUserGroupRaspoloziveRole_Select", conn);

    request.addParameter('PkApplicationUserGroup', TYPES.Int, req.query.PkApplicationUserGroup);
    request.addParameter('PkUsera', TYPES.Int, req.query.PkUsera);
    db.execStoredProc(request, conn, res, '[]');
});

router.post('/PridodajRoluGrupi', function (req, res) {
    var conn = db.createConnection();
    var request = db.createRequest("Sigurnost.spApplicationUserGroupApplicationRole_Insert", conn);

    request.addParameter('PkApplicationRole', TYPES.Int, req.body.PkApplicationRole);
    request.addParameter('PkApplicationUserGroup', TYPES.Int, req.body.PkApplicationUserGroup);
    request.addParameter('PkUsera', TYPES.Int, req.body.PkUsera);
    request.addOutputParameter('PkApplicationUserGroupApplicationRole', TYPES.Int);

    db.execStoredProc(request, conn, res, '{}');
});

router.delete('/IzbrisiRoluIzGrupe', function (req, res) {
    var conn = db.createConnection();
    var request = db.createRequest("Sigurnost.spApplicationUserGroupApplicationRole_Delete", conn);

    request.addParameter('PkApplicationUserGroupApplicationRole', TYPES.Int, req.body.PkApplicationUserGroupApplicationRole);
    request.addParameter('PkUsera', TYPES.Int, req.body.PkUsera);

    db.execStoredProc(request, conn, res, '{}');
});

// --------------------------------------------------------
// USER LOGIN
// --------------------------------------------------------
router.post('/userLogin', function (req, res) {
    login.dbLogin(req, res);
});

module.exports = router;