var greska = [], //objekt koji vraca gresku
    outputvalue = [], //output parametar kod azuriranja podataka
    appConfig = require('./config.js');
//konfiguracija za pooler
// var config = {
//     server: appConfig.databaseServer,
//     instanceName: appConfig.instancename,
//     userName: appConfig.username,
//     password: appConfig.password,
//     // If you're on Azure, you will need this:
//     options: {
//         encrypt: true,
//         database: appConfig.databaseName
//     }
// };


function createConnection() {

    var config = {
        server: 'localhost',
        instanceName: appConfig.instancename,
        // Ovo je za novije verzije Tediousa logiranje


        authentication: {
          type: "default",
          options: {
            userName: appConfig.username,
            password: appConfig.password,
          }
        },


        // // If you're on Azure, you will need this:
        options: {
            encrypt: true,
            database: appConfig.databaseName
        }
    };
    var Connection = require('tedious').Connection;
    var connection = new Connection(config);

    return connection;
}

function createRequest(query, connection) {
    var Request = require('tedious').Request;
    var req =
        new Request(query,
            function (err, rowCount) {
                if (err) {
                    //logger.log('error in db.createRequest', err);
                }
                connection && connection.close();
            });
    return req;
}

function execStoredProc(query, connection, output, defaultContent, callback) {
    var request = query;
    greska = [];
    outputvalue = [];
    if (typeof query == "string") {
        request = this.createRequest(query, connection);
    }
    var empty = true;
    request.on('row', function (columns) {
        empty = false;
        output.write(columns[0].value);
    });
    request.on('done', function (rowCount, more, rows) {
        if (empty) {
            output.write(defaultContent);
        }
        output.end();
    });

    request.on('doneProc', function (rowCount, more, returnStatus, rows) {
        //saljem callback za slanje pretplate
        if (typeof callback === 'function' && greska.length === 0) {
            callback(outputvalue);
        }
        if (greska.length > 0) {
            output.status(500).write(JSON.stringify(greska[0]));
        }
        if (empty && greska.length == 0) {
            //output.write(defaultContent);
            output.write(JSON.stringify(outputvalue));
        }
        outputvalue = [];
        output.end();
    });
    request.on('returnValue', function (parameterName, value, metadata) {
        var temp = {};
        temp[parameterName] = value;
        outputvalue.push(temp);
    });

    connection.on('errorMessage', function (err) {
        //logger.log('error in db.execStoredProc.connection on', err);
        if (err) {
            greska.push(err);
        }

    });
    connection.on('connect', function (err) {
        if (err) {
            console.log(err);
        }
        connection.callProcedure(request);
    });
};

function execStoredProcNoJSONLocalResults(dbRequest, connection, req, res, callback) {

    var request = dbRequest;
  
    var resultData = [];
  
    greska = [];
  
   
  
    request.on('row', function (columns) {
  
      var rowObject = {};
  
      columns.forEach(function (column) {
  
        rowObject[column.metadata.colName] = column.value;
  
      });
  
      resultData.push(rowObject);
  
    });
  
   
  
    request.on('doneProc', function () {
  
   
  
      if (greska.length === 0) {
  
        if (typeof callback === "function") {
  
          callback({
  
            resultStatus: '0',
  
            resultData: resultData
  
          }, req, res);
  
        } else {
  
          return {
  
            resultStatus: '0',
  
            resultData: resultData
  
          }
  
        }
  
      } else {
  
        if (typeof callback === "function") {
  
          callback(greska, req, res);
  
        } else {
  
          return {
  
            greska
  
          }
  
        }
  
      }
  
    });
  
   
  
    connection.on('errorMessage', function (err) {
  
      console.log('error errorMessage', err.message);
  
      //logger.log('error in db.execStoredProc.connection on', err);
  
      if (err) {
  
   
  
        greska = {
  
          resultStatus: '9999',
  
          resultData: err
  
        };
  
        return {
  
          resultStatus: err.message,
  
          resultData: err
  
        }
  
      }
  
    });
  
   
  
    connection.on('connect', function (err) {
  
      if (err) {
  
        // logger.log('error in db.execStoredProcPredmetLocal.connection.on:', err);
  
        console.log('error connect');
  
      }
  
      connection.callProcedure(request);
  
    });
  
  };



module.exports.createConnection = createConnection;
module.exports.createRequest = createRequest;
module.exports.execStoredProc = execStoredProc;
module.exports.execStoredProcNoJSONLocalResults = execStoredProcNoJSONLocalResults;
