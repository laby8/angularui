// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express = require('express'), // call express
    // app = express(), // define our app using express
    bodyParser = require('body-parser'),
    appConfig = require('./config.js'),
    app = require('express')();


app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    next();
});
// parse various different custom JSON types as JSON
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({
    extended: true
})); // support encoded bodies za pristup s Postmanom.



app.use('/api', require('./api'));
var port = appConfig.applicationPort; // set our port


// development error handler
// will print stacktrace
app.use(function (err, req, res, next) {
    console.log('ERROR: ', err.message, err.status);
    res.status(err.status || 500).end();
});


module.exports = app;
// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Server started ' + port);
