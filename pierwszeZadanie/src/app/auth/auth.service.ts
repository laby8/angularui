import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loggedIn = new BehaviorSubject<boolean>(false);

  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }

  constructor(private router: Router,private http: HttpClient) { }

  login(username, password) {
return this.http.post<any>('http://localhost:8090/api/userLogin',{"userName":username,"password":password})
.pipe(map(user=> {
  console.log(user);
  if(user && user.loginToken) {
    localStorage.setItem('loggedUser',user.PkUsera);
    this.loggedIn.next(true);
      this.router.navigate(['/users']);
  }
  else
  this.logout();
}))
  }
  logout() {
    this.loggedIn.next(false);
    this.router.navigate(['/']);
  }
}
