import { Component, OnInit } from '@angular/core';
import { User } from '../users/user';
import { Group } from './group';
import { GroupService } from './group.service';
import { Role } from '../roles/role';

@Component({
  selector: 'app-group-detail',
  templateUrl: './group-detail.component.html',
  styleUrls: ['../styles/details.scss']
})
export class GroupDetailComponent implements OnInit {

  availableUsers: User[];
  groupUsers: User[];
  draggedUser: User;
  cols: any[];

  availableRoles: Role[];
  groupRoles: Role[];
  draggedRole: Role;

  selectedGroup: Group;
  areMembers: boolean = true;

  constructor(private groupService: GroupService) { }

  ngOnInit() {
    this.getUsers();
    this.getColumns();
    this.getRoles();
    this.selectedGroup = this.groupService.getSelectedGroup();
  }

  dragStart(event, user: User) {
    this.draggedUser = user;
  }
  dragEnd(event) {
    this.draggedUser = null;
  }

  addUser() {
    this.groupService.addUserToGroup(this.draggedUser).subscribe(() => this.getUsers());
  }
  deleteUser() {
    this.groupService.deleteUserFromGroup(this.draggedUser).subscribe(() => this.getUsers());
  }

  getUsers() {
    this.groupService.getGroupUsers().subscribe(users => this.groupUsers = users);
    this.groupService.getAvailableUsers().subscribe(users => this.availableUsers = users);
  }

  getColumns() {
    this.cols = this.groupService.getColumns();
  }

  getRoles() {
    this.groupService.getGroupRoles().subscribe(roles => {this.groupRoles = roles});
    this.groupService.getAvailableRoles().subscribe(roles =>{ this.availableRoles = roles;console.log(roles)});;
  }

  dragStartRole(event, role: Role) {
    this.draggedRole = role;
  }
  dragEndRole(event) {
    this.draggedRole = null;
  }

  addRole() {
    console.log(this.draggedRole);
    this.groupService.addRoleToGroup(this.draggedRole.rolePk).subscribe(() => this.getRoles());
  }

  deleteRole() {
    this.groupService.deleteRoleFromGroup(this.draggedRole).subscribe(() => this.getRoles());
  }

  toggleMembers(){
    if(this.areMembers == true)
      this.areMembers = false;
    else
      this.areMembers = true;
      console.log(this.availableRoles);
  }

  showMembers(){
    return this.areMembers;
  }
}
