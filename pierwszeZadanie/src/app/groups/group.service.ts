import { Injectable } from '@angular/core';
import { Group } from './group';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';
import { User } from '../users/user';
import { Role } from '../roles/role';

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  selectedGroup: Group;
  groups: Group[];
  cols: any[];

  private groupUrl: string = 'http://localhost:8090/api/GrupeKorisnika';
  private groupUsersUrl: string = 'http://localhost:8090/api//GrupeKorisnikaPripadajuciKorisnici';
  private notGroupUsersUrl: string = 'http://localhost:8090/api/GrupeKorisnikaRaspoloziviKorisnici';
  
  private addUserUrl: string = 'http://localhost:8090/api/KorisnikDodajUGrupu';
  private deleteUserUrl: string = 'http://localhost:8090/api/KorisnikIzbrisiIzGrupe';
  
  private availableRolesUrl: string = 'http://localhost:8090/api/GrupeKorisnikaRaspoloziveRole';
  private groupRolesUrl: string = 'http://localhost:8090/api/GrupeKorisnikaPripadajuceRole';
  private roleAddUrl: string = 'http://localhost:8090/api/PridodajRoluGrupi';
  private roleDeleteUrl: string = 'http://localhost:8090/api/IzbrisiRoluIzGrupe'
  constructor(private http: HttpClient) { }

  getColumns(): any[] {
    this.cols = [
      { field: 'groupPk', header: 'Group Pk' },
      { field: 'groupId', header: 'Group id' },
      { field: 'groupName', header: 'Group name' },
      { field: 'insertedBy', header: 'Inserted By'},
      { field: 'insertionDate', header: 'Insertion Date' },
      { field: 'changedBy', header: 'Changed By' },
      { field: 'changeDate', header: 'Modification Date' },
      { field: 'rowVersion', header: 'Row Version' },
      { field: 'active', header: 'Active' },
      { field: 'visible', header: 'Visible'}
      
    ];
    return this.cols;
  }

  getRoleColumns(): any[] {
    var columns = [  { field: 'rolePk', header: 'Primary Key' },
    { field: 'roleName', header: 'Role Name' },
    { field: 'roleDescription', header: 'Description' }]
    return columns;
  }

  getGroups(): Observable<Group[]> {
    return this.http.get<Group[]>(this.groupUrl).pipe(catchError(this.handleError));
  }
  postGroup(group: Group): Observable<Group> {
    return this.http.post<Group>(this.groupUrl, group).pipe(catchError(this.handleError));
   }
   editGroup(group: Group): Observable<Group> {
     return this.http.put<Group>(this.groupUrl, group).pipe(catchError(this.handleError));
   }
   deleteGroup(group: Group): Observable<{}> {
    return this.http.request('delete',this.groupUrl,{body: group}).pipe(catchError(this.handleError));
   }

   getGroupUsers(): Observable<User[]>{
     return this.http.get<User[]>(this.groupUsersUrl + `?PkApplicationUserGroup=${this.selectedGroup.groupPk}`)
     .pipe(catchError(this.handleError));
   }
   getAvailableUsers(): Observable<User[]>{
    return this.http.get<User[]>(this.notGroupUsersUrl + `?PkApplicationUserGroup=${this.selectedGroup.groupPk}`)
    .pipe(catchError(this.handleError));
   }

   addUserToGroup(user: User): Observable<User>{
    return this.http.post<User>(this.addUserUrl,{ "KorisnikPkUsera": user.userId, "groupPk": this.selectedGroup.groupPk, "PkUsera": user.userId }).pipe(catchError(this.handleError));
   }
   deleteUserFromGroup(user: User): Observable<Object>{
    var sendable = { "PkApplicationUserApplicationUserGroup": user.PkApplicationUserApplicationUserGroup, "PkUsera": user.userId };
    return this.http.request('delete', this.deleteUserUrl, { body: sendable });
   }

   getGroupRoles(): Observable<Role[]>{
     return this.http.get<Role[]>(this.groupRolesUrl + `?PkApplicationUserGroup=${this.selectedGroup.groupPk}`).pipe(catchError(this.handleError));
   }
   getAvailableRoles(): Observable<Role[]>{
     return this.http.get<Role[]>(this.availableRolesUrl + `?PkApplicationUserGroup=${this.selectedGroup.groupPk}`).pipe(catchError(this.handleError));
   }

   addRoleToGroup(rolePk: number): Observable<Role>{
     return this.http.post<Role>(this.roleAddUrl,
      {"PkApplicationRole": rolePk,"PkApplicationUserGroup": this.selectedGroup.groupPk,"PkUsera": 10 });
   }
   deleteRoleFromGroup(role: Role): Observable<Object>{
    var sendable = { "PkApplicationUserGroupApplicationRole": role.PkApplicationUserGroupApplicationRole, "PkUsera": 10 };
     return this.http.request('delete',this.roleDeleteUrl,{body : sendable});
   }



  setSelectedGroup(group: Group): any {
    this.selectedGroup = group;
  }
  getSelectedGroup(): Group {
    return this.selectedGroup;
  }

  private handleError(err: HttpErrorResponse) {
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      errorMessage = `${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }
}
