

export interface Group {
    groupPk: number;
    groupId: string;
    groupName: string;
    insertedBy: string;
    insertionDate: string;
    changedBy: string;
    changeDate: string;
    active: boolean;
    visible: boolean;
    rowVersion: string;
    PkApplicationUserApplicationUserGroup: number;
}