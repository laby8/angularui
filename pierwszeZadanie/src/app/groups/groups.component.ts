import { Component, OnInit } from '@angular/core';
import { GroupService } from './group.service';
import { Group } from './group';
import { DialogService, ConfirmationService, MessageService } from 'primeng/api';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['../styles/tables.scss'],
  providers: [DialogService, ConfirmationService,MessageService]
})
export class GroupsComponent implements OnInit {

  confirmationMessage: string = 'Are you sure you want to delete this group?';
  groups: Group[];
  cols: any[];
  selectedGroup: Group;
  groupToSend: Group;
  displayMainDialog: boolean = false;
  displayDeleteDialog: boolean = false;
  displayAddDialog: boolean = false;
  displayEditDialog: boolean = false;
  headerMessage: string;
  group: Group;
  activeSearch: boolean;
  visibleSearch: boolean;

  constructor(private groupService: GroupService,
    public dialogService: DialogService,
    private confirmationService: ConfirmationService,
    private translate: TranslateService,
    private messageService: MessageService) {

     }

  ngOnInit() {
    this.groupToSend = <Group>{"visible":false,"active":false,"groupPk":3,"changeDate":"","changedBy":"","groupId":"","groupName":"","insertedBy":"","insertionDate":"","rowVersion":"","SuperUserDaNe":false,"PkApplicationUserApplicationUserGroup":null};
    this.groupService.getGroups().subscribe(groups => { this.groups = groups;},error =>  this.messageService.add({key: 'error', severity:'error', summary: 'Error occured: ', detail: error}));
    this.cols = this.groupService.getColumns();
  }

  isActiveCheck(field: string): boolean {
    if (field === 'active')
      return true;
    return false;
  }

  isVisibleCheck(field: string): boolean {
    if (field === 'visible')
      return true;
    return false;
  }

  checkForBoolean(field: String): boolean {
    if (field === 'active' || field === 'visible')
      return false;
    return true;
  }

  deleteGroupYes() {
    const index: number = this.groups.indexOf(this.selectedGroup);
    if (index > -1) {
      this.groups.splice(index, 1);
      this.displayMainDialog = false;
      this.displayDeleteDialog = false;
    }
    this.groupService.deleteGroup(this.selectedGroup).subscribe(() => console.log("success"),error =>  this.messageService.add({key: 'error', severity:'error', summary: 'Error occured: ', detail: error}));
  }
  deleteGroupNo() {
    this.displayDeleteDialog = false;
  }
  deleteClick() {
    this.confirmationService.confirm({
      header: 'Warning: ',
      message: this.confirmationMessage,
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.deleteGroupYes();
      }
    });
  }

  addGroup(){
    this.groupToSend.groupPk=5;
    this.groupService.postGroup(this.groupToSend).subscribe(() => {
    this.groupService.getGroups().subscribe(groups => this.groups = groups);this.messageService.add({key: 'addSuccess', severity:'success', summary: 'Group added', detail: 'Group has been successfully inserted into the database'});
  },error =>  this.messageService.add({key: 'error', severity:'error', summary: 'Error occured: ', detail: error}));
    this.displayAddDialog = false;
}


checkForAddButton(): boolean {
  if(this.groupToSend.groupName != ""){
  return false;
  }
  return true;
}

  updateGroup(){
    this.selectedGroup.groupName = "siemaneczko";
    this.groupService.editGroup(this.selectedGroup).subscribe(() => {
      this.groupService.getGroups().subscribe(groups => this.groups = groups);
      },error =>  this.messageService.add({key: 'error', severity:'error', summary: 'Error occured: ', detail: error}));
     this.displayAddDialog = false;
  }

  onDblClick(sth): void {
    this.displayMainDialog = true;
    this.selectedGroup = sth;
    console.log(this.selectedGroup);
    this.groupService.setSelectedGroup(this.selectedGroup);

  }

  showEditDialog(): void {
    this.groupToSend = this.selectedGroup;
    this.displayAddDialog = true;
    this.headerMessage = 'Edit';
    this.displayEditDialog = true;

  }

  setEditDialogFalse(): void {
    this.displayEditDialog = false;
  }

  showAddDialog(): void {
    this.groupToSend = <Group>{"visible":false,"active":false,"groupPk":3,"changeDate":"","changedBy":"","groupId":"","groupName":"","insertedBy":"","insertionDate":"","rowVersion":"","SuperUserDaNe":false,"PkApplicationUserApplicationUserGroup":null};
    this.displayAddDialog = true;
    this.displayEditDialog = false;
    this.headerMessage = 'Add';

  }


}
