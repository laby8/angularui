import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserDetailComponent } from './users/user-detail.component';
import { UsersComponent } from './users/users.component';
import { TableModule } from 'primeng/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { FieldsetModule } from 'primeng/fieldset';
import { AppRoutingModule } from './app-routing.module';
import { GroupsComponent } from './groups/groups.component';
import {CardModule} from 'primeng/card';
import { TabMenuModule } from 'primeng/tabmenu';
import { DragDropModule } from 'primeng/dragdrop';
import { FormsModule } from '@angular/forms';
import {ToastModule} from 'primeng/toast';

import { InputSwitchModule } from 'primeng/inputswitch';
import { InputTextModule } from 'primeng/inputtext';
import { CheckboxModule } from 'primeng/checkbox';


import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { GroupDetailComponent } from './groups/group-detail.component';


import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';


export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    UserDetailComponent,
    UsersComponent,
    GroupsComponent,
    GroupDetailComponent,
    LoginComponent,
    HeaderComponent
  ],
  imports: [
    CommonModule,
    TableModule,
    BrowserAnimationsModule,
    DialogModule,
    ButtonModule,
    FieldsetModule,
    AppRoutingModule,
    FormsModule,
    InputSwitchModule,
    InputTextModule,
    ToastModule,
    DragDropModule,
    ConfirmDialogModule,
    CheckboxModule,
    HttpClientModule,
    TabMenuModule,
    CardModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  exports: [
    AppRoutingModule,
    HttpClientModule,
    HeaderComponent
  ]
})
export class GroupsAndUsersModule {

}
