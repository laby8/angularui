import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { UserService } from '../users/user.service';
import { User } from '../users/user';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: ['../styles/header.scss']
})
export class HeaderComponent implements OnInit {
  loggedUser: User;
  isLoggedIn$: Observable<boolean>;
  items: MenuItem[];
  activeItem: MenuItem;
  constructor(
    private translate: TranslateService,private authService: AuthService, private userService : UserService) { 
    this.items = [
      { label: 'Users', icon: 'pi pi-user', routerLink: ['/users'] },
      { label: 'Groups', icon: 'pi pi-users', routerLink: ['/groups'] },
    ];
    this.activeItem = this.items[0];

    translate.addLangs(['en', 'cr']);
    translate.setDefaultLang('en');

    const browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/en|cr/) ? browserLang : 'en');
  }


  useLanguage(language: string) {
    this.translate.use(language);
  }
  

  ngOnInit() {
    this.isLoggedIn$=this.authService.isLoggedIn;
    this.userService.getLoggedUser().subscribe(user => this.loggedUser = user[0]);
  }

  onLogout(){
    this.authService.logout();
  }

}
