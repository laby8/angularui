export interface Role {
    rolePk: number;
    roleName: string;
    roleDescription: string;
    PkApplicationUserGroupApplicationRole: number;
}
