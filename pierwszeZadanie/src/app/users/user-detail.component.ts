import { Component, OnInit } from '@angular/core';
import { User } from './user';
import { UserService } from './user.service';
import { GroupService } from '../groups/group.service';
import { Group } from '../groups/group';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['../styles/details.scss']
})
export class UserDetailComponent implements OnInit {

  userGroups: Group[];
  otherGroups: Group[];

  draggedGroup: Group;
  selectedUser: User;

  cols: any[];

  constructor(private userService: UserService, private groupService: GroupService) { }

  ngOnInit() {
    this.selectedUser = this.userService.getSelectedUser();
    this.cols = this.userService.getColumns();
    this.getGroups();
  }

  dragStart(event, group: Group) {
    this.draggedGroup = group;
  }
  dragEnd(event) {
    this.draggedGroup = null;
  }

  addGroup() {
    this.userService.addUserGroup(this.draggedGroup).subscribe(() => { this.getGroups() });
  }
  deleteGroup() {
    this.userService.deleteUserGroup(this.draggedGroup).subscribe(() => { this.getGroups() });
  }
  
  getGroups() {
    this.userService.getUserGroups().subscribe(groups => this.userGroups = groups);
    this.userService.getNotUserGroups().subscribe(groups => this.otherGroups = groups);

  }

}
