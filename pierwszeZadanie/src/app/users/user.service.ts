import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { User } from './user';
import { Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { Group } from '../groups/group';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  private selectedUser: User;
  users: User[];
  cols: any[];
  private userUrl = 'http://localhost:8090/api/Korisnik';
  private userGroupsUrl = 'http://localhost:8090/api/KorisnikPripadajuceGrupe';
  private notUserGroupsUrl = 'http://localhost:8090/api/KorisnikRaspoloziveGrupe';
  private addGroupUrl = 'http://localhost:8090/api/KorisnikDodajUGrupu';
  private deleteGroupUrl = 'http://localhost:8090/api/KorisnikIzbrisiIzGrupe';
  private passwordChangeUrl = 'http://localhost:8090/api/IzmjenaLozinke';
  constructor(private http: HttpClient) { }

  getColumns(): any[] {
    this.cols = [
      { field: 'userId', header: 'User Id' },
      { field: 'name', header: 'Name' },
      { field: 'surname', header: 'Surname' },
      { field: 'username', header: 'Username' },
      { field: 'businessPartnerName', header: 'Bussiness partner name' },
      { field: 'businessPartnerId', header: 'Bussiness partner id' },
      { field: 'email', header: 'Email' },
      { field: 'changedBy', header: 'Changed By' },
      { field: 'changedDate', header: 'Modification Date' },
      { field: 'insertedBy', header: 'Inserted By' },
      { field: 'insertionDate', header: 'Insertion Date' },
      { field: 'rowVersion', header: 'Row Version' },
      { field: 'active', header: 'is Active' },
      { field: 'passwordChangeNeeded', header: 'Password change needed' }
    ];
    return this.cols;
  }
  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.userUrl).pipe(
      tap(data => console.log('All: ' + JSON.stringify(data))), catchError(this.handleError)
    );
  }

  getLoggedUser(): Observable<User>{
    return this.http.get<User>(this.userUrl + `?KorisnikPkUsera=${localStorage.getItem('loggedUser')}`).pipe(
    tap(data => console.log('balblaa: ' + JSON.stringify(data))), catchError(this.handleError));
  }
  postUser(user: User,insertPk: number): Observable<User> {
    return this.http.post<User>(this.userUrl, {user, insertPk}).pipe(catchError(this.handleError));
  }
  editUser(user: User,changePk: number): Observable<User> {
    return this.http.put<User>(this.userUrl, {user, changePk}).pipe(catchError(this.handleError));
  }
  deleteUser(user: User,deletePk: number): Observable<{}> {
    return this.http.request('delete', this.userUrl, { body: {user,deletePk} }).pipe(catchError(this.handleError));
  }

  getUserGroups(): Observable<Group[]> {
    return this.http.get<Group[]>(this.userGroupsUrl + `?PkUsera=${this.selectedUser.userId}&KorisnikPkUsera=${this.selectedUser.userId}`).pipe(
      tap(data => console.log('All: ' + JSON.stringify(data))), catchError(this.handleError)
    );
  }
  getNotUserGroups(): Observable<Group[]> {
    return this.http.get<Group[]>(this.notUserGroupsUrl + `?PkUsera=${this.selectedUser.userId}&KorisnikPkUsera=${this.selectedUser.userId}`).pipe(
      tap(data => console.log('All: ' + JSON.stringify(data))), catchError(this.handleError)
    );
  }

  deleteUserGroup(group: Group): Observable<Object> {
    var sendable = { "PkApplicationUserApplicationUserGroup": group.PkApplicationUserApplicationUserGroup, "PkUsera": this.selectedUser.userId };
    return this.http.request('delete', this.deleteGroupUrl, { body: sendable });
  }
  addUserGroup(group: Group): Observable<Group> {
    return this.http.post<Group>(this.addGroupUrl, { "KorisnikPkUsera": this.selectedUser.userId, "groupPk": group.groupPk, "PkUsera": this.selectedUser.userId });
  }

  private handleError(err: HttpErrorResponse) {
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      errorMessage = `An error occurred: ${err.error.message}`;
    }
    else {
      errorMessage = `${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }

  setSelectedUser(user: User) {
    this.selectedUser = user;
  }
  getSelectedUser(): User {
    return this.selectedUser;
  }

  changePassword(user: User,oldPassword: String): Observable<User> {
return this.http.put<User>(this.passwordChangeUrl, {user, oldPassword}).pipe(catchError(this.handleError));
  }

}




