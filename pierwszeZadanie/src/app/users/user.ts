
export interface User {
    active: boolean;
    passwordChangeNeeded: boolean;
    surname: string;
    name: string;
    username: string;
    businessPartnerName: string;
    businessPartnerId: string;
    email: string;
    userId: number;
    password: string;
    insertedBy: string;
    insertionDate: string;
    changedBy: string;
    changedDate: string;
    rowVersion: string;
    PkApplicationUserApplicationUserGroup: number;
}