import { Component, OnInit } from '@angular/core';
import { UserService } from './user.service';
import { User } from './user';
import { DialogService, ConfirmationService, MessageService } from 'primeng/api';

import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['../styles/tables.scss'],
  providers: [DialogService, ConfirmationService,MessageService]
})
export class UsersComponent implements OnInit {
  confirmationMessage: string = 'Are you sure you want to delete this user?';
  users: User[];
  cols: any[];
  selectedUser: User;
  userToSend: User;
  loggedUser: User;
  displayMainDialog: boolean = false;
  displayDeleteDialog: boolean = false;
  displayAddDialog: boolean = false;
  displayEditDialog: boolean = false;
  displayPasswordDialog: boolean = false;
  addButtonDisable: boolean;
  headerMessage: string;
  activeSearch: boolean;
  passSearch: boolean;
  confirmPassword: string;
  oldPassword: String;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";


  constructor(private userService: UserService,
    public dialogService: DialogService,
    private confirmationService: ConfirmationService,
    private translate: TranslateService,
    private messageService: MessageService,
    private authService: AuthService) { }
    

  ngOnInit(): void {
    this.addButtonDisable = true;
  this.userService.getUsers().subscribe(users => { this.users = users;},error =>  this.messageService.add({key: 'error', severity:'error', summary: 'Error occured: ', detail: error})
);
    this.cols = this.userService.getColumns();
    this.userToSend = <User>{};
    this.loggedUser = <User>{};
    this.userService.getLoggedUser().subscribe(user => {this.loggedUser = user[0]; this.checkForPassword();});
    console.log(this.displayPasswordDialog);
  }

addUser(){
  if(this.isProperUser(this.userToSend)){
 this.userService.postUser(this.userToSend,this.loggedUser.userId).subscribe(() => {
  this.userService.getUsers().subscribe(users => this.users = users);this.messageService.add({key: 'addSuccess', severity:'success', summary: 'User added', detail: 'User has been successfully inserted into the database'});
  },error =>  this.messageService.add({key: 'error', severity:'error', summary: 'Error occured: ', detail: error}));
  this.displayAddDialog = false;
  }
}

isProperUser(user: User): boolean{
this.checkEmail(user);
this.checkPassword(user);
return true;
}

checkPassword(user: User): boolean{
  if(user.password != this.confirmPassword){
    this.messageService.add({key: 'wrongEmail', severity:'error', summary: 'Passwords do not match', detail: 'Please put identical passwords'});
    return false;
    }
}

checkEmail(user: User): boolean{
  if(!user.email.match(this.emailPattern)){
    this.messageService.add({key: 'wrongEmail', severity:'error', summary: 'Email is not correct', detail: 'Please put there right email'});
    return false;
    }
    return true;
}

updateUser(){
  if(this.checkEmail(this.userToSend)){
    this.userService.editUser(this.userToSend,this.loggedUser.userId).subscribe(() => {
     this.userService.getUsers().subscribe(users => this.users = users);
     },error =>  this.messageService.add({key: 'error', severity:'error', summary: 'Error occured: ', detail: error}));
     this.displayAddDialog = false;
     this.messageService.add({key: 'addSuccess', severity:'success', summary: 'User modified', detail: 'User has been successfully modified'});
   }
}

  deleteUserYes() {
    this.userToSend = this.selectedUser;
    const index: number = this.users.indexOf(this.selectedUser);
    if (index > -1) {
      this.users.splice(index, 1);
      this.displayMainDialog = false;
      this.displayDeleteDialog = false;
    }
    this.userService.deleteUser(this.selectedUser,this.loggedUser.userId).subscribe(() => console.log("success"),error =>  this.messageService.add({key: 'error', severity:'error', summary: 'Error occured: ', detail: error}));
  }

  isActiveCheck(field: string): boolean {
    if (field === 'active')
      return true;
    return false;
  }
  passwordChangeCheck(field: string): boolean {
    if (field === 'passwordChangeNeeded')
      return true;
    return false;
  }
  deleteUserNo() {
    this.displayDeleteDialog = false;
  }
  deleteClick() {
    this.confirmationService.confirm({
      header: 'Warning: ',
      message: this.confirmationMessage,
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.deleteUserYes();
      }
    });
  }

  checkForBoolean(field: String): boolean {
    if (field === 'active' || field === 'passwordChangeNeeded')
      return false;
    return true;
  }

  editDialogCheck(bla): string {
    return this.displayEditDialog ? bla : '';
  }

  onDblClick(sth): void {
    this.displayMainDialog = true;
    this.selectedUser = sth;
    this.userService.setSelectedUser(sth);
  }

  showEditDialog(): void {
    this.userToSend = this.selectedUser;
    this.displayAddDialog = true;
    this.headerMessage = 'Edit';
    this.displayEditDialog = true;
  }

  showAddDialog(): void {
    this.userToSend = {"active":false,"passwordChangeNeeded":false,"surname":"","name":"","username":"","businessPartnerName":"","businessPartnerId":"","email":"","insertedBy":"","insertionDate":"","changedBy":"","changedDate":"","userId":parseInt(localStorage.getItem('loggedUser')),"rowVersion":"","password":"","PkApplicationUserApplicationUserGroup" : null};
    this.displayAddDialog = true;
    this.displayEditDialog = false;
    this.headerMessage = 'Add';
  }

  checkForAddButton(): boolean {
    if(this.userToSend.username != ""){
    return false;
    }
    return true;
  }

  checkForPassword() {
    if(this.loggedUser.passwordChangeNeeded == true){
      this.oldPassword=this.loggedUser.password;
    this.displayPasswordDialog = true;
    this.loggedUser.password="";
    }
    else
    this.displayPasswordDialog = false;
    }

    changePassword(){
      if(this.checkPassword(this.loggedUser) != false){
      this.userService.changePassword(this.loggedUser,this.oldPassword).subscribe();
      this.displayPasswordDialog = false;
      this.authService.logout();
    }
    }

    checkForEdit(): boolean{
      if(this.headerMessage == 'Edit')
        return true;
        return false;
    }

} // koniec klasy
